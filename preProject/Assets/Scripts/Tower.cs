﻿using UnityEngine;
using System.Collections;

public class Tower: MonoBehaviour {

	// Enemy 객체를 Inspector 창에서 노출 하도록 public 지시자 이용.
	public GameObject enemy;
    public GameObject bullet;
    public Transform firePos;

	void Update () {
		LookEnemy ();
		FireBullet ();
	}

	// 적을 바라 보도록 한다.
	void LookEnemy()
	{
		// Enemy 가 존재할 경우(할당된 enemy가 있을 경우) 
		if (enemy) {
			// Enemy 를 바라 보도록 LookAt 함수를 호출한다.
			transform.LookAt (enemy.transform);
		}
        else {
            // 생성된 enemy 를 tag 로 검색하여 할당한다.		
            enemy = GameObject.FindGameObjectWithTag("Enemy");
        }
    }

    void FireBullet()
	{
		//Fire1 버튼 (left ctrl, mouse left) 클릭되었을 때 이벤트 처리.
		if (enemy && Input.GetButtonDown ("Fire1")) {
			// 총알 생성.
			GameObject tmpBullet = Instantiate (bullet);
			// 총알 위치 지정.
			tmpBullet.transform.position = firePos.position;
			// 총알 각도 지정.
			Vector3 targetVector = enemy.transform.position - firePos.position;
			tmpBullet.transform.forward = targetVector.normalized;
		}
	}
}
