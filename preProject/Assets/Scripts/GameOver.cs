﻿using UnityEngine;
// SceneManager 클레스를 사용하기위한 네임스페이스
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOver : MonoBehaviour {

    public static bool gameOver = false;

    void OnEnable()
    {
        Camera main = Camera.main;

        transform.position = main.transform.position + main.transform.forward * 5f;
        transform.LookAt(main.transform);
        transform.eulerAngles += new Vector3(180f, 0f, 180f);

        gameOver = true;
    }

	// 게임을 재 실행 한다.
	public void RestartGame()
	{
        // 씬 로딩
        SceneManager.LoadScene ("Demo");

        gameOver = false;
    }
}
