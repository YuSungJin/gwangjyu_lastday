﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TILE_TYPE	{ NONE=-1, ROAD=0, WALL, WATER }

public class TileManager : MonoBehaviour
{
	[SerializeField]
	private	GameObject	tile_prefab;
	private	List<Tile>	tile_list;

	public void Create_All_Tiles_In_File(int width, int height, List<int> _tile_type)
	{
		tile_list = new List<Tile>(width*height);
		for ( int y = 0; y < height; ++ y )
		{
			for ( int x = 0; x < width; ++ x )
			{
				int		idx = y * width + x;
				Vector3 pos = new Vector3(-(width*.5f-1.0f)+x*2.0f, .0f, (height*.5f-1.0f)-y*2.0f);
				Create_Tile((TILE_TYPE)_tile_type[idx], pos);
			}
		}
	}
	public void Create_Tile(TILE_TYPE type, Vector3 pos)
	{
		GameObject clone		= Instantiate(tile_prefab) as GameObject;
		clone.transform.parent	= this.transform;
		clone.name				= "Tile";

		Tile tile = clone.GetComponent<Tile>();
		tile.Init(type, pos);

		tile_list.Add(tile);
	}
	public Tile Get_Tile(int idx)	{ return tile_list[idx]; }
}

