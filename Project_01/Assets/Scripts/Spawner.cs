﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

    public GameObject block;
    public float begin = 0f;
    public float cycle = 0.5f;
    public float probability = 70f;

    void Start()
    {
        InvokeRepeating("Spawn", begin, cycle);
    }

    void Spawn()
    {
        float seed = Random.Range(0f, 100f);
        if (seed < probability)
        {
            GameObject obj = Instantiate(block);
            obj.transform.position = transform.position;
        }
    }
}
