﻿using UnityEngine;
using System.Collections;

public class SpawnMover : MonoBehaviour
{

    public Player player;

    void Update()
    {
        transform.position += Vector3.forward * Time.deltaTime * player.speed;
    }
}
