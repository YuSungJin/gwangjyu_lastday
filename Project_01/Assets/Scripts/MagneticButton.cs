﻿using UnityEngine;
using System.Collections;

public class MagneticButton : MonoBehaviour
{
    magneticClick magClick = new magneticClick();

    void Start()
    {
        Input.compass.enabled = true;
    }

    void Update()
    {
        magClick.magUpdate(Input.acceleration, Input.compass.rawVector);
        if (magClick.clicked())
            SendMessage("OnMagneticClicked");
    }

    //public bool magnetDetectionEnabled = true;

    //void Start()
    //{
    //    CardboardMagnetSensor.SetEnabled(magnetDetectionEnabled);
    //    // Disable screen dimming:
    //    Screen.sleepTimeout = SleepTimeout.NeverSleep;
    //}

    //void Update()
    //{
    //    if (!magnetDetectionEnabled) return;
    //    if (CardboardMagnetSensor.CheckIfWasClicked())
    //    {
    //        SendMessage("OnMagneticClicked");
    //        CardboardMagnetSensor.ResetClick();
    //    }
    //}
}