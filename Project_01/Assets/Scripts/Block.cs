﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour
{

    public float speed = 3f;
    Rigidbody rigid;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        Destroy(gameObject, 30f);

        GetComponent<Renderer>().material.SetColor("_Color", Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f));
    }

    void Update()
    {
        Vector3 dest = transform.position;
        dest.y = 0f;

        transform.position = Vector3.Lerp(transform.position, dest, Time.deltaTime * speed);
    }
}
