﻿using UnityEngine;
using System.Collections;

public class Comet : MonoBehaviour
{

    public float power = 2000f;

    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(Vector3.back * power);

        GetComponent<Renderer>().material.SetColor("_Color", Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f));
    }
}
