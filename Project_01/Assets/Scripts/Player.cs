﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    public float speed = 5f;
    public float jump = 333f;

    Rigidbody rigid;
    public int jumpCount = 0;

	void Start () {

        rigid = GetComponent<Rigidbody>();

        destPos = transform.position.x;
    }
	
	void Update () {

        //Vector3 forward = Vector3.forward;
        //Vector3 worldForward = Camera.main.transform.TransformDirection(forward);
        //Vector3 dir = worldForward.normalized * speed;

        //transform.position += dir * Time.deltaTime;
        transform.position += Vector3.forward * Time.deltaTime * speed;

        if (Input.GetButtonDown("Jump") && jumpCount > 0)
        {
            rigid.AddForce(Vector3.up * jump);
            jumpCount = 0;
        }

        Tilt();
    }

    float destPos;
    bool moving = false;
    void Tilt()
    {
        if (moving == false)
        {
            Vector3 right = Camera.main.transform.TransformDirection(Vector3.right) + transform.position;
            Vector3 left = Camera.main.transform.TransformDirection(Vector3.left) + transform.position;

            float differ = Mathf.Abs(right.y - left.y);
            if (right.y > left.y && differ > 1f)
            {
                destPos -= 1f;
                StartCoroutine("TiltMoving");
            }
            if (right.y < left.y && differ > 1f)
            {
                destPos += 1f;
                StartCoroutine("TiltMoving");
            }
        }

        float x = Mathf.Lerp(transform.position.x, destPos, Time.deltaTime);
        Vector3 pos = transform.position;
        pos.x = x;
        transform.position = pos;
    }

    IEnumerator TiltMoving()
    {
        if (moving == true)
            yield break;

        moving = true;

        yield return new WaitForSeconds(1f);

        moving = false;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Block")
        {
            jumpCount = 1;
        }

        if (col.gameObject.tag == "Comet")
        {
            rigid.AddForce(Random.onUnitSphere * 999f);
        }
    }

    void OnJump()
    {
        if (jumpCount > 0)
        {
            rigid.AddForce(Vector3.up * jump);
            jumpCount = 0;
        }
    }
}
