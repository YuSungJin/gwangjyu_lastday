﻿using UnityEngine;
using System.Collections;

public class Dead : MonoBehaviour
{

    public GameObject player;
    float height;

    void Start()
    {
        height = transform.position.y;
    }

    void Update()
    {
        Vector3 pos = player.transform.position;
        pos.y = height;
        transform.position = pos;

        transform.eulerAngles = Vector3.zero;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Application.LoadLevel("Demo");
        }
    }
}
