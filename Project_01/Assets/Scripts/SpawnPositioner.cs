﻿using UnityEngine;
using System.Collections;

public class SpawnPositioner : MonoBehaviour
{

    public GameObject player;
    Vector3 offset;

    void Awake()
    {
        offset = transform.position - player.transform.position;
    }

    void Update()
    {
        //transform.position = player.transform.position + offset;
        Vector3 pos = player.transform.position + offset;
        pos.x = 0f;
        transform.position = pos;
    }
}
